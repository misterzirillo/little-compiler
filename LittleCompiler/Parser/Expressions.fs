namespace LittleCompiler.Parser

open FParsec.CharParsers
open FParsec.Primitives
open FParsec
open Token

module Expressions =

    type LittleOperationExpression =
        | AddOperation of LittleExpression * LittleExpression
        | SubtractOperation of LittleExpression * LittleExpression
        | MultOperation of LittleExpression * LittleExpression
        | DivOperation of LittleExpression * LittleExpression

    and LittleExpression =
        | OperationExpression of LittleOperationExpression
        | ValueLiteralExpression of LittleValueLiteral
        | IdentifierExpression of LittleIdentifierLiteral
        | FunctionCallExpression of LittleIdentifierLiteral * LittleExpression list

    /// Parse an identifier literal
    let private pIdentifierLiteral = pIdentifier |>> IdentifierExpression    

    /// Parses a float or int literal
    let private pIntOrFloat =
        let chooseIntOrFloat =
            // deconstruct the value of the previous parser
            function
            | IntLiteral x -> x |> IntLiteral |> NumberLiteral
            | FloatLiteral (x,y) -> FloatLiteral (x,y) |> NumberLiteral

        pIntOrFloatLiteral |>> (chooseIntOrFloat >> ValueLiteralExpression)

    let private opp = new OperatorPrecedenceParser<LittleExpression,_,_>()

    let private infixOperator op prec map =
        opp.AddOperator(InfixOperator (op, spaces, prec, Associativity.Left, map))

    infixOperator "+" 1 (fun x y -> AddOperation (x, y) |> OperationExpression)
    infixOperator "-" 1 (fun x y -> SubtractOperation (x, y) |> OperationExpression)
    infixOperator "*" 2 (fun x y -> MultOperation (x, y) |> OperationExpression)
    infixOperator "/" 2 (fun x y -> DivOperation (x, y) |> OperationExpression)

    let private betweenparens = between ((pstring "(") .>> spaces) (pstring ")")

    let pExpression = opp.ExpressionParser

    let private pFunctionCall =
        let openParens = pOperatorParensOpen .>> spaces
        let closeParens = spaces .>> pOperatorParensClose
        let delim = (pstring ",") .>> spaces
        let insideParens = between openParens closeParens (sepBy pExpression delim)
        let outsideParens = pIdentifier .>> spaces

        outsideParens .>>. insideParens |>> FunctionCallExpression <?> "function call"

    opp.TermParser <- choice [
        pIntOrFloat .>> spaces
        (betweenparens pExpression) .>> spaces
        attempt <| (pFunctionCall .>> spaces)
        pIdentifierLiteral .>> spaces
    ]