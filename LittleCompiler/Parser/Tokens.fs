namespace LittleCompiler.Parser

open FParsec.CharParsers
open FParsec.Primitives
open LittleCompiler

module Token =

    // Token definitions for parser

    // Delineate token types
    type LittleTokenType =
        | OperatorType
        | KeywordType
        // Each literal is their own type
        // According to .out files provided at least
        | IntLiteralType
        | FloatLiteralType
        | StringLiteralType
        | IdentifierType
        | CommentLiteralType

    type LittleVarTypeLiteral =
        | INT
        | STRING
        | FLOAT

    /// Represents an instance of a type literal as in `TYPE symbol ;`
    type LittleAnyTypeLiteral =
        | VOID
        | VarType of LittleVarTypeLiteral
    
    /// Represents an instance of a symbol, eg `TYPE symbol ;`
    type LittleIdentifierLiteral = string

    /// Represents an instance of a string literal, eg `"string"`
    type LittleStringLiteral = string

    /// Represents a numberic type literal
    type LittleNumberLiteral =
        | IntLiteral        of int
        | FloatLiteral      of float * string

    /// Represents any inline literal value
    type LittleValueLiteral =
        | NumberLiteral     of LittleNumberLiteral
        | StringLiteral     of LittleStringLiteral

    type LittleComparisonOperator =
        | GreaterThan
        | LessThan
        | GreaterThanEq
        | LessThanEq
        | Eq 
        | NotEq

    /// Represents an inline operation
    type LittleOperator =
        | ParensOpen
        | ParensClose
        | SemiColon
        | Plus
        | Minus
        | Times
        | Divide
        | Assign
        | Comma 
        | Comparison of LittleComparisonOperator

    /// Reperesents an instance of a restricted keyword
    type LittleKeyword =
        | Program
        | Begin 
        | End 
        | Function 
        | Read 
        | Write 
        | If 
        | Else 
        | EndIf
        | While 
        | EndWhile 
        | Continue 
        | Break 
        | Return 

    /// Represents every literal syntax
    type LittleLiteral =
        | TypeLiteral       of LittleAnyTypeLiteral
        | ValueLiteral     of LittleValueLiteral
        | IdentifierLiteral of LittleIdentifierLiteral
        | CommentLiteral    of string

    /// Represents any token
    type LittleToken =
        | Literal of LittleLiteral
        | Operator of LittleOperator
        | Keyword of LittleKeyword

    let private tokenProps = dict[
        Program |> Keyword,    ("PROGRAM",     KeywordType);
        Begin |> Keyword,      ("BEGIN",       KeywordType);
        End |> Keyword,        ("END",         KeywordType);
        Function |> Keyword,   ("FUNCTION",    KeywordType);
        Read |> Keyword,       ("READ",        KeywordType);
        Write |> Keyword,      ("WRITE",       KeywordType);
        If |> Keyword,         ("IF",          KeywordType);
        Else |> Keyword,       ("ELSE",        KeywordType);
        EndIf |> Keyword,      ("ENDIF",       KeywordType);
        While |> Keyword,      ("WHILE",       KeywordType);
        EndWhile |> Keyword,   ("ENDWHILE",    KeywordType);
        Continue |> Keyword,   ("CONTINUE",    KeywordType);
        Break |> Keyword,      ("BREAK",       KeywordType);
        Return |> Keyword,     ("RETURN",      KeywordType);
        INT     |> VarType |> TypeLiteral |> Literal, ("INT",     KeywordType);
        VOID    |> TypeLiteral |> Literal, ("VOID",    KeywordType);
        STRING  |> VarType |> TypeLiteral |> Literal, ("STRING",  KeywordType);
        FLOAT   |> VarType |> TypeLiteral |> Literal, ("FLOAT",   KeywordType);

        ParensOpen |> Operator,     ("(",   OperatorType);
        ParensClose |> Operator,    (")",   OperatorType);
        SemiColon |> Operator,      (";",   OperatorType);
        Plus |> Operator,           ("+",   OperatorType);
        Minus |> Operator,          ("-",   OperatorType);
        Times |> Operator,          ("*",   OperatorType);
        Divide |> Operator,         ("/",   OperatorType);
        Assign |> Operator,         (":=",  OperatorType);
        LessThan |> Comparison |> Operator,       ("<",   OperatorType);
        GreaterThan |> Comparison |> Operator,    (">",   OperatorType);
        LessThanEq |> Comparison |> Operator,     ("<=",  OperatorType);
        GreaterThanEq |> Comparison |> Operator,  (">=",  OperatorType);
        Eq |> Comparison |> Operator,             ("=",   OperatorType);
        NotEq |> Comparison |> Operator,          ("!=",  OperatorType);
        Comma |> Operator,          (",",   OperatorType);
    ]

    let getValue t : string = 
        // Handle tokens with a variable value, don't play well with dict
        match t with
        | Literal l ->
            match l with
            | ValueLiteral v ->
                match v with
                | NumberLiteral n ->
                    match n with
                    | IntLiteral        x -> string x
                    | FloatLiteral      (_, x) -> x
                | StringLiteral     x -> x
            | IdentifierLiteral x -> x
            | CommentLiteral    x -> x
            | TypeLiteral t -> fst(tokenProps.[t |> TypeLiteral |> Literal])
        // All other tokens have a reflexive value - token identifier is itself
        | _ -> fst(tokenProps.[t])

    let getType t : LittleTokenType = 
        match t with
        | Literal l ->
            match l with
            | ValueLiteral v ->
                match v with
                | NumberLiteral n ->
                    match n with
                    | IntLiteral        _ -> IntLiteralType
                    | FloatLiteral      _ -> FloatLiteralType
                | StringLiteral     _ -> StringLiteralType
            | IdentifierLiteral _ -> IdentifierType
            | CommentLiteral    _ -> CommentLiteralType
            | TypeLiteral _ -> KeywordType
        | _ -> snd(tokenProps.[t])

    let getTypeName t = 
        match getType t with
        | OperatorType          -> "OPERATOR"
        | KeywordType           -> "KEYWORD"
        | IntLiteralType        -> "INTLITERAL"
        | FloatLiteralType      -> "FLOATLITERAL"
        | StringLiteralType     -> "STRINGLITERAL"
        | IdentifierType        -> "IDENTIFIER"
        | CommentLiteralType    -> "COMMENTLITERAL"


    // Parser glue code from here down

    let private getTypeParser (token: LittleAnyTypeLiteral) : Parser<LittleAnyTypeLiteral, SymbolTableDict> = 
        stringReturn (token |> TypeLiteral |> Literal |> getValue) token

    let private getVarTypeParser (token: LittleVarTypeLiteral) : Parser<LittleVarTypeLiteral, SymbolTableDict> = 
        stringReturn (token |> VarType |> TypeLiteral |> Literal |> getValue) token

    let private getKeywordParser (kw: LittleKeyword) : Parser<LittleKeyword, SymbolTableDict> =
        stringReturn (kw |> Keyword |> getValue) kw

    let private getOperatorParser (op: LittleOperator) : Parser<LittleOperator, SymbolTableDict> =
        stringReturn (op |> Operator |> getValue) op

    let private getCompOperatorParser (op: LittleComparisonOperator) : Parser<LittleComparisonOperator, SymbolTableDict> =
        stringReturn (op |> Comparison |> Operator |> getValue) op

    // keyword parsers
    let pKeywordProgram = 
        getKeywordParser Program
        .>> updateUserState(fun s -> 
            s.AddTable "GLOBAL"

            s
        )
    let pKeywordBegin =     getKeywordParser Begin
    let pKeywordEnd =       getKeywordParser End
    let pKeywordFunction =  getKeywordParser Function
    let pKeywordRead =      getKeywordParser Read
    let pKeywordWrite =     getKeywordParser Write
    let pKeywordIf = 
        getKeywordParser If
        .>> updateUserState(fun s ->
            // Push new SymbolTable when "IF" encountered
            // Each variable seen in the if statement goes into this table
            // Not name dependent, so it can live here
            s.AddTable "BLOCK"

            s
        )
    let pKeywordElse =
        getKeywordParser Else
        .>> updateUserState(fun s ->
            s.AddTable "BLOCK" // Push new block table

            s
        )
    let pKeywordEndIf =     getKeywordParser EndIf
    let pKeywordWhile =
        getKeywordParser While
        .>> updateUserState(fun s ->
            s.AddTable "BLOCK"

            s
        )
    let pKeywordEndWhile =  getKeywordParser EndWhile
    let pKeywordContinue =  getKeywordParser Continue
    let pKeywordBreak =     getKeywordParser Break
    let pKeywordReturn =    getKeywordParser Return

    let pKeywordInt =       getVarTypeParser INT
    let pKeywordFloat =     getVarTypeParser FLOAT 
    let pKeywordVoid =      getTypeParser VOID 
    let pKeywordString =    getVarTypeParser STRING 

    let pVarTypeLiteral =
        choice [
            pKeywordFloat
            pKeywordInt
            pKeywordString
        ]

    let pAnyTypeLiteral =
        choice [
            pKeywordVoid
            pVarTypeLiteral |>> VarType
        ]

    // operator parsers
    let pOperatorParensOpen =       getOperatorParser ParensOpen
    let pOperatorParensClose =      getOperatorParser ParensClose
    let pOperatorSemiColon =        getOperatorParser SemiColon
    let pOperatorPlus =             getOperatorParser Plus
    let pOperatorMinus =            getOperatorParser Minus
    let pOperatorTimes =            getOperatorParser Times
    let pOperatorDivide =           getOperatorParser Divide
    let pOperatorAssign =           getOperatorParser Assign
    let pOperatorComma =            getOperatorParser Comma

    let pOperatorGreaterThanEq =    getCompOperatorParser GreaterThanEq
    let pOperatorGreaterThan =      getCompOperatorParser GreaterThan
    let pOperatorLessThanEq =       getCompOperatorParser LessThanEq
    let pOperatorLessThan =         getCompOperatorParser LessThan
    let pOperatorEq =               getCompOperatorParser Eq
    let pOperatorNotEq =            getCompOperatorParser NotEq

    // literal parsers
    let pStringLiteral =
        let normalChar = satisfy (fun c -> c <> '\\' && c <> '"')
        let unescape c = match c with
                         | 'n' -> '\n'
                         | 'r' -> '\r'
                         | 't' -> '\t'
                         | c   -> c
        let escapedChar = pstring "\\" >>. (anyOf "\\nrt\"" |>> unescape)
        between (pstring "\"") (pstring "\"")
                (manyChars (normalChar <|> escapedChar))

    let private numberFormat =
        NumberLiteralOptions.AllowMinusSign
        ||| NumberLiteralOptions.AllowFraction

    let pIntOrFloatLiteral =
        numberLiteral numberFormat "number"
        |>> function
            | x when x.IsInteger -> int x.String |> IntLiteral
            | y -> FloatLiteral (double y.String, y.String)

    let private mapIntOrFloatToken = NumberLiteral >> ValueLiteral

    let pIntOrFloatToken = pIntOrFloatLiteral |>> mapIntOrFloatToken

    let private pCommentLiteral =
        skipString "--" >>. restOfLine true |>> CommentLiteral

    let whitespaceOrComment =
        let manycomment = sepEndBy pCommentLiteral spaces
        spaces .>> opt manycomment

    let pIdentifier : Parser<string, SymbolTableDict> =
        // get strings for all reserved words, transform into negative-lookahead
        let notReserved = 
            tokenProps
            |> Seq.filter (fun kvp ->
                let _, ttype = kvp.Value    
                match ttype with KeywordType -> true | _ -> false)
            |> Seq.map (fun kvp -> kvp.Value |> fst |> (sprintf "(?!%s)"))
            |> Seq.reduce (+)
        
        sprintf @"%s[a-zA-Z]\w*" notReserved |> regex <?> "identifier"

    let private raiseValueLiteral = ValueLiteral >> Literal

    let pToken : Parser<LittleToken, SymbolTableDict> = 
        choice [
            pStringLiteral |>> (StringLiteral >> ValueLiteral >> Literal)
            pCommentLiteral |>> Literal // try to parse comment before minus
            pOperatorMinus |>> Operator // try to parse minus before numbers
            pIntOrFloatToken |>> Literal
            pKeywordProgram |>> Keyword
            pKeywordBegin |>> Keyword
            pKeywordEndIf |>> Keyword // ENDIF, ENDWHILE, etc need to be before END because fparsec is greedy
            pKeywordEndWhile |>> Keyword
            pKeywordEnd |>> Keyword
            pKeywordFunction |>> Keyword
            pKeywordRead |>> Keyword
            pKeywordWrite |>> Keyword
            pKeywordIf |>> Keyword
            pKeywordElse |>> Keyword
            pKeywordWhile |>> Keyword
            pKeywordContinue |>> Keyword
            pKeywordBreak |>> Keyword
            pKeywordReturn |>> Keyword
            pKeywordInt |>> (VarType >> TypeLiteral >> Literal)
            pKeywordVoid |>> (TypeLiteral >> Literal)
            pKeywordString |>> (VarType >> TypeLiteral >> Literal)
            pKeywordFloat |>> (VarType >> TypeLiteral >> Literal)
            pOperatorAssign |>> Operator
            pOperatorComma |>> Operator
            pOperatorDivide |>> Operator
            pOperatorGreaterThanEq |>> (Comparison >> Operator)
            pOperatorGreaterThan |>> (Comparison >> Operator)
            pOperatorNotEq |>> (Comparison >> Operator)
            pOperatorParensClose |>> Operator
            pOperatorParensOpen |>> Operator
            pOperatorPlus |>> Operator
            pOperatorSemiColon |>> Operator
            pOperatorTimes |>> Operator
            pOperatorLessThanEq |>> (Comparison >> Operator)
            pOperatorLessThan |>> (Comparison >> Operator)
            pOperatorEq |>> (Comparison >> Operator)
            pIdentifier |>> IdentifierLiteral |>> Literal
        ]
