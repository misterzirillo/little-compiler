namespace LittleCompiler.Parser

open Token
open Statement

open FParsec.Primitives
open FParsec.CharParsers
open LittleCompiler

module Functions =

    type LittleFunctionArgDeclaration = LittleVarTypeLiteral * LittleIdentifierLiteral

    type LittleFunctionDeclaration =
        LittleAnyTypeLiteral * LittleIdentifierLiteral * LittleFunctionArgDeclaration list * LittleStatement list

    let private pFunctionArgDeclaration =
        pVarTypeLiteral .>> spaces .>>. pIdentifier

    let private pListDelimiter = skipChar ',' .>> spaces
    let private pFunctionArgDeclarationList = sepEndBy (pFunctionArgDeclaration .>> spaces) pListDelimiter

    let private betweenParens p =
        between
            (pOperatorParensOpen .>> spaces)
            (spaces >>. pOperatorParensClose)
            p

    let pFunctionDeclaration =
        
        // New symbol table for the current function being parsed
        // All arguments to the function will be placed in this table
        let mutable funcName = ""
        let mutable funcArgs: (LittleVarTypeLiteral * string) list = []

        let funcTypeId = 
            pKeywordFunction >>. spaces1 >>. 
            pAnyTypeLiteral .>> spaces1 .>>. 
            pIdentifier
            |>> (fun (ftype, fname) ->
                funcName <- fname
                (ftype, fname)
            )

        let funcArgs = 
            betweenParens pFunctionArgDeclarationList
            |>> (fun fargs -> 
                funcArgs <- fargs
                fargs
            )
            .>> updateUserState(fun s ->
                // Push new table for this funciton
                s.AddTable funcName

                // Add all arguments to the table
                funcArgs |> List.iter(fun (argType, argName) ->
                    ignore <| s.AddVariable (string argType) argName
                )

                s
            )

        let funcBody = pKeywordBegin >>. spaces1 >>. pAnyStatementList .>> pKeywordEnd
        

        funcTypeId .>> spaces .>>. funcArgs .>> spaces .>>. funcBody
        |>> ((fun (((ftype, fname), fargss), fbody) -> 
            (ftype, fname, fargss, fbody)) >> LittleFunctionDeclaration
        )
