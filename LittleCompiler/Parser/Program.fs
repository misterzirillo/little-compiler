namespace LittleCompiler.Parser

open Token
open Functions
open Statement
open FParsec
open LittleCompiler

module Program =

    type LittleProgram = {
        programName : LittleIdentifierLiteral
        globalVars : LittleVariableStatement list 
        functions : LittleFunctionDeclaration list
    }

    let pProgram : Parser<LittleProgram, SymbolTableDict> =
        let pProgramName : Parser<string, SymbolTableDict> = spaces >>. pKeywordProgram >>. spaces1 >>. pIdentifier
        let pGlobalVars : Parser<LittleVariableStatement list, SymbolTableDict> = sepEndBy (pGlobalStringAssignment <|> pVarDeclaration) whitespaceOrComment
        let pFunctionDecls = sepEndBy pFunctionDeclaration whitespaceOrComment

        spaces >>. pProgramName
        .>> spaces1 .>> pKeywordBegin
        .>> spaces1 .>>. pGlobalVars
        .>>. pFunctionDecls .>> pKeywordEnd
        |>> fun ((pname, pvars), pfns) ->
            { 
                programName = pname
                globalVars = pvars
                functions = pfns
            }
