namespace LittleCompiler.Parser

open Token
open Expressions
open FParsec.Primitives
open FParsec.CharParsers
open LittleCompiler

module Statement =

    type LittleComparison = LittleExpression * LittleComparisonOperator * LittleExpression

    type LittleVariableStatement =
        | VarDeclaration of LittleVarTypeLiteral * LittleIdentifierLiteral list
        | GlobalString of LittleIdentifierLiteral * LittleStringLiteral

    type LittleStatement =
        | Declaration of LittleVariableStatement
        | Assignment of LittleIdentifierLiteral * LittleExpression
        | ReadStatement of LittleIdentifierLiteral list
        | WriteStatement of LittleIdentifierLiteral list
        | Return of LittleExpression
        | ControlStatement of LittleFlowControlStatement

    and LittleFlowControlStatement =
        // comparison, main statements, else statements
        | IfStatement of LittleComparison * LittleStatement list * LittleStatement list option
        | WhileStatement of LittleComparison * LittleStatement list

    let private pTerminatingColon p =
        p .>> spaces .>> pOperatorSemiColon

    let private pListDelimiter = skipChar ',' .>> spaces
    let private pIdList = (sepEndBy1 (pIdentifier .>> spaces) pListDelimiter)

    let pVarDeclaration =
        let mutable varType = ""
        let mutable identifiers: string list = []
        pVarTypeLiteral .>> spaces .>>. pIdList
        |> pTerminatingColon
        |>> ((fun (ftype, fidentifiers) -> 
            varType <- string ftype
            identifiers <- fidentifiers

            (ftype, fidentifiers)) >> VarDeclaration
        )
        .>> updateUserState(fun s ->
            // Geeky currying
            let addVarTypeCur = s.AddVariable varType

            // Add all identifiers associated with the type
            identifiers |> List.iter(fun identifier ->
                addVarTypeCur identifier
            )

            s
        )

    let pGlobalStringAssignment =
        let mutable name = ""
        let mutable value = ""
        pKeywordString .>> spaces >>. pIdentifier .>> spaces .>> pOperatorAssign .>> spaces .>>. pStringLiteral
        |> pTerminatingColon
        |>> ((fun (fidentifier, fvalue) -> 
            name <- fidentifier
            value <- fvalue.Replace("\n", "\\n")

            (fidentifier, fvalue)) >> GlobalString
        )
        .>> updateUserState(fun s -> 
            s.AddStringVariable name value

            s
        )

    let pVarAssignment =
        pIdentifier .>> spaces .>> pOperatorAssign .>> spaces .>>. pExpression
        |> pTerminatingColon
        |>> Assignment

    let private betweenParens p =
        between
            (pOperatorParensOpen .>> spaces)
            (spaces >>. pOperatorParensClose)
            p

    let pReadStatement =
        pKeywordRead >>. spaces >>. betweenParens pIdList 
        |> pTerminatingColon
        |>> ReadStatement

    let pWriteStatement =
        pKeywordWrite >>. spaces >>. betweenParens pIdList 
        |> pTerminatingColon
        |>> WriteStatement

    let pReturnStatement =
        pKeywordReturn >>. spaces >>. pExpression
        |> pTerminatingColon
        |>> Return

    let pAnyStatement, pAnyStatementRef = createParserForwardedToRef<LittleStatement, SymbolTableDict>()

    let private pConditional =
        let pConditionalOp =
            choice [
                pOperatorGreaterThanEq
                pOperatorGreaterThan
                pOperatorLessThanEq
                pOperatorLessThan
                pOperatorNotEq
                pOperatorEq
            ]

        pExpression .>> spaces .>>. pConditionalOp .>> spaces .>>. pExpression
        |>> fun ((e1, o), e2) -> (e1, o, e2)

    let private getConditional pKeywordIn pBetween pKeywordOut =
        pKeywordIn >>. spaces >>. betweenParens pConditional .>> whitespaceOrComment .>>. pBetween .>> pKeywordOut

    let pAnyStatementList =
        sepEndBy pAnyStatement whitespaceOrComment

    let pIfStatement =
        let pElseStatement = pKeywordElse >>. spaces >>. pAnyStatementList
        let insideIf = pAnyStatementList .>>. opt pElseStatement
        getConditional pKeywordIf insideIf pKeywordEndIf
        |>> ((fun (c, (i, e)) -> (c, i, e)) >> IfStatement)

    let pWhileStatement =
        getConditional pKeywordWhile pAnyStatementList pKeywordEndWhile
        |>> WhileStatement

    do pAnyStatementRef := 
        choice [
            pGlobalStringAssignment |>> Declaration
            pVarDeclaration |>> Declaration
            pIfStatement |>> ControlStatement
            pWhileStatement |>> ControlStatement
            pVarAssignment
            pReadStatement
            pWriteStatement
            pReturnStatement
        ]