namespace LittleCompiler.Parser

open FParsec.CharParsers
open FParsec.Primitives

module Comment =

    let pComment : Parser<unit,unit> =
        pstring "--" >>. skipRestOfLine true