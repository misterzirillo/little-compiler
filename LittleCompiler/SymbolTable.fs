﻿namespace LittleCompiler

open System.Text
open System.Collections.Generic


type SymbolTable() = 

    // Dictionary of type -> identifiers
    let _variables = new Dictionary<string, string list>()

    do
        _variables.Add("STRING",    [])
        _variables.Add("INT",       [])
        _variables.Add("FLOAT",     [])

    member this.Variables with get() = _variables

    member this.AddVariable (varType: string) (varName: string) = 
        // Fails loudly if the type isn't string, int or float
        _variables.[varType] <- _variables.[varType] @ [varName]
        
    member this.AddStringVariable (strName: string) (strVal: string) =
        _variables.["STRING"] <- _variables.["STRING"] @ [strName]

    member this.GetVariableType (varName: string) =
        if      List.contains varName _variables.["STRING"] then "STRING"
        elif    List.contains varName _variables.["INT"]    then "INT"
        elif    List.contains varName _variables.["FLOAT"]  then "FLOAT"
        else    ""


// Used when parsing
type SymbolTableDict() = 
    inherit Dictionary<string, SymbolTable>()

    let mutable _numBlockScopes: int = 0
    let mutable _curTableName = ""

    // Add variable to the current table
    member this.AddVariable (varType: string) (varName: string) =
        if _curTableName <> "" then
            this.[_curTableName].AddVariable varType varName

    // Add string variable to the current 
    member this.AddStringVariable (varType: string) (varName: string) =
        if _curTableName <> "" then
            this.[_curTableName].AddStringVariable varType varName

    member this.AddTable tableName =
        let realName = 
            // Automatically increment block scope numbers
            if tableName = "BLOCK" then (
                _numBlockScopes <- _numBlockScopes + 1
                sprintf "%s %d" tableName _numBlockScopes
            )
            else tableName

        this.Add(realName, new SymbolTable())
        _curTableName <- realName
    
    member this.CurrentTable with get() = this.[_curTableName]
    member this.CurrentTableName with get() = _curTableName


// Used during code generation
type SymbolTableStack(tableDict: SymbolTableDict) =
    let _dict = tableDict
    // Initialize stack with global table
    let mutable _stack: SymbolTable list = [tableDict.["GLOBAL"]]
    let mutable _curBlockNum = 0

    member this.PushTable tableName =
        _stack <- [_dict.[tableName]] @ _stack

    member this.PopTable =
        match _stack with
        | _::tail -> _stack <- tail
        | _ -> ()

    member this.GetNextBlockTable =
        _curBlockNum <- _curBlockNum + 1
        _dict.[sprintf "BLOCK %d" _curBlockNum]

    member this.GetVarType varName = 
        let rec searchVar (tableList: SymbolTable list) varName =
            match tableList with
            | [] -> ""
            | head::xs ->
                let varType = head.GetVariableType varName
                if varType <> "" then varType
                else searchVar xs varName

        searchVar _stack varName
                