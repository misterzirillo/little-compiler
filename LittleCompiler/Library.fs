﻿namespace LittleCompiler

open FParsec
open LittleCompiler.Parser.Program
open LittleCompiler.Parser.Statement
open LittleCompiler.Parser.Token
open LittleCompiler
open System.Text
open LittleCompiler.Parser.Expressions

module LittleCompiler =

    /// Given a filename produce an AST
    let parseMicroFile filename =
        let result = runParserOnFile pProgram (new SymbolTableDict()) filename System.Text.Encoding.UTF8
        match result with
        | Success (r, s, _) -> (r, s)
        | Failure (e, _, _) -> failwith e

    let getTinyFromAST (prog: LittleProgram) (symbolTables: SymbolTableDict) =
        let instBuilder = StringBuilder()
        let addInstruction (inst: string) =
            ignore <| instBuilder.Append(inst).Append('\n')

        let tableStack = new SymbolTableStack(symbolTables)

        let mutable nTempRegisters = -1
        let getTempRegister () =
            nTempRegisters <- nTempRegisters + 1
            sprintf "r%d" nTempRegisters

        // Returns the register where the result of the expression is stored
        let rec compileExpression (expr: LittleExpression) =
            match expr with
            | ValueLiteralExpression (literal) ->
                match literal with
                | NumberLiteral (number) ->
                    // Get temp register to load literal into
                    let register = getTempRegister ()

                    // Figure out what to do with the literal
                    match number with
                    | IntLiteral (numberInt) ->
                        addInstruction (sprintf "move %s %s" (string numberInt) register)
                        ("INT", register)
                    | FloatLiteral (_, numberFloat) ->
                        addInstruction (sprintf "move %s %s" numberFloat register)
                        // Give name of register and type back
                        ("FLOAT", register)
                | StringLiteral (s) ->
                    // TODO: Actually handle temp strings?

                    // Pseudocode
                    // let tempName = getTempStringName
                    // addInstruction (sprintf "string %s \"%s\"" tempName (string s))
                    // (tempName, "STRING")
                    ("STRING", "") // for now, don't care
            | IdentifierExpression (id) ->
                let register = getTempRegister ()
                // Just load the variable into a register
                addInstruction (sprintf "move %s %s" id register)
                // Return the register name and the type
                (tableStack.GetVarType id, register)
            | FunctionCallExpression (id, args) ->
                // TODO: Actually unpack function call, do jmp statements etc.
                ("", "") // Don't care for now
            | OperationExpression (opExpr) ->
                // Get instruction info
                let (instName, (lType, lResult), (rType, rResult)) =
                    // TODO: Pattern match and catch NumberLiteral
                    //  to avoid loading into a register unnecessarily
                    match opExpr with
                    | AddOperation      (leftExpr, rightExpr) ->
                        ("add", compileExpression leftExpr, compileExpression rightExpr)
                    | SubtractOperation (leftExpr, rightExpr) ->
                        ("sub", compileExpression leftExpr, compileExpression rightExpr)
                    | MultOperation     (leftExpr, rightExpr) ->
                        ("mul", compileExpression leftExpr, compileExpression rightExpr)
                    | DivOperation      (leftExpr, rightExpr) ->
                        ("div", compileExpression leftExpr, compileExpression rightExpr)
                
                // TODO: Verify behavior of int/float mixing by operation type
                // Placeholder just care about what register is being assigned to
                match (lType, rType) with
                | ("INT", _) ->
                    addInstruction (sprintf "%si %s %s" instName rResult lResult)
                    ("INT", lResult)
                | ("FLOAT", _) ->
                    addInstruction (sprintf "%sr %s %s" instName rResult lResult)
                    ("FLOAT", lResult)
                | _ ->
                    addInstruction ("## BAD EXPRESSION TYPE")
                    ("", "")

        let compileStatement (statement: LittleStatement) =
            match statement with
            | Declaration (d) ->
                match d with
                | VarDeclaration (_, identifiers) ->
                    identifiers |> List.iter(fun id ->
                        addInstruction (sprintf "var %s" id)
                    )
                | GlobalString (id, content) ->
                    addInstruction (sprintf "str %s \"%s\"" id content)
            | Assignment (id, expression) ->
                // Get register where expression result is stored
                let (eType, eRegister) = compileExpression expression
                addInstruction (sprintf "move %s %s" eRegister id)
            | ReadStatement (identifiers) ->
                identifiers |> List.iter(fun id ->
                    let idType = tableStack.GetVarType id
                    match idType with
                    | "INT" ->
                        addInstruction (sprintf "sys readi %s" id)
                    | "FLOAT" ->
                        addInstruction (sprintf "sys readr %s" id)
                    | "STRING" -> // Don't know if this can even happen
                        addInstruction (sprintf "sys reads %s" id)
                )
            | WriteStatement (identifiers) ->
                identifiers |> List.iter(fun id ->
                    match tableStack.GetVarType id with
                    | "INT" ->
                        addInstruction (sprintf "sys writei %s" id)
                    | "FLOAT" ->
                        addInstruction (sprintf "sys writer %s" id)
                    | "STRING" ->
                        addInstruction (sprintf "sys writes %s" id)
                    | _ -> 
                        addInstruction ("## BAD WRITE TYPE")
                )
            | LittleStatement.Return (id) ->
                () // Do we even need to deal with this?
            | ControlStatement (control) ->
                match control with
                | IfStatement (comparison, mainStatements, elseStatements) ->
                    addInstruction ("## IF STATEMENT NYI")
                | WhileStatement (comparison, bodyStatements) ->
                    addInstruction ("## WHILE STATEMENT NYI")
            | _ -> addInstruction ("## UNMATCHED STATEMENT")

        // Declare all named global variables in tiny - easy
        prog.globalVars
        |> List.iter(fun var ->
            match var with
            | VarDeclaration (_, identifiers) ->
                identifiers |> List.iter(fun id ->
                    addInstruction (sprintf "var %s" id)
                )
            | GlobalString (id, content) ->
                let escapedContent = content.Replace("\n", "\\n")
                addInstruction (sprintf "str %s \"%s\"" id escapedContent)
        )

        // Add function body instructions
        prog.functions
        |> List.iter(fun (ftype, fname, fargs, fstatements) ->
            tableStack.PushTable fname
            fstatements
            |> List.iter(fun statement -> compileStatement statement)
            tableStack.PopTable
        )

        // I don't think there's any other logic for this
        // Just at the end of all statements?
        addInstruction "sys halt"

        // Return all compiled instructions as string
        instBuilder.ToString ()
