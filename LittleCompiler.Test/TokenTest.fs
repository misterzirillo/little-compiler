namespace LittleCompiler.Test

open NUnit.Framework
open LittleCompiler.Parser.Token
open FParsec.CharParsers
open LittleCompiler

module TokenTest =

    let passFail result =
        match result with
        | Ok _ -> Assert.Pass()
        | Error e -> Assert.Fail(e)

    let equateTokens a b =
        if a = b then Ok () else Error (sprintf "%O != %O" a b)

    let equateTokenss (a: LittleToken list) (b: LittleToken list) =
        let indexeda = List.indexed a
        Seq.zip indexeda b 
        |> Seq.fold
            (fun state ((ai, a), b) ->
                match state with
                | Error e -> Error e
                | Ok _ ->
                    match equateTokens a b with
                    | Ok _ -> Ok ()
                    | Error e -> Error (sprintf "Token #%d: %s" ai e))
            (Ok ())
        |> passFail


    [<Test>]
    let Fibonacci () =
        let inTokens = readTokensInFile "Assets/Step1/inputs/fibonacci.micro"
        let outTokens = readTokensOutFile "Assets/Step1/outputs/fibonacci.out"
        equateTokenss inTokens outTokens

    [<Test>]
    let Loop () =
        let inTokens = readTokensInFile "Assets/Step1/inputs/loop.micro"
        let outTokens = readTokensOutFile "Assets/Step1/outputs/loop.out"
        equateTokenss inTokens outTokens

    [<Test>]
    let Sqrt () =
        let inTokens = readTokensInFile "Assets/Step1/inputs/sqrt.micro"
        let outTokens = readTokensOutFile "Assets/Step1/outputs/sqrt.out"
        equateTokenss inTokens outTokens

    [<Test>]
    let Nested () =
        let inTokens = readTokensInFile "Assets/Step1/inputs/nested.micro"
        let outTokens = readTokensOutFile "Assets/Step1/outputs/nested.out"
        equateTokenss inTokens outTokens

    let mapNumberLiteral = LittleValueLiteral.NumberLiteral >> ValueLiteral >> Literal
    let getValueOp = Operator >> getValue
    let getValueKw = Keyword >> getValue
    let getTokenTypeOp = Operator >> getTypeName
    let getTokenTypeKw = Keyword >> getTypeName

    [<Test>]
    let GetValue () = 
        // Keywords
        Assert.AreEqual(getValueKw Program, "PROGRAM")
        Assert.AreEqual(getValueKw Function, "FUNCTION")

        // Operators
        Assert.AreEqual(getValueOp SemiColon, ";")
        Assert.AreEqual(getValueOp (Comparison GreaterThanEq), ">=")

        // Literals
        Assert.AreEqual(getValue (IntLiteral 1 |> mapNumberLiteral), string 1)
        Assert.AreEqual(getValue (FloatLiteral (1.0, "1.0") |> mapNumberLiteral), "1.0")

    [<Test>]
    let GetTypeName () = 
        // Keywords
        Assert.AreEqual(getTokenTypeKw Program, "KEYWORD")
        Assert.AreEqual(getTokenTypeKw Function, "KEYWORD")

        // Operators
        Assert.AreEqual(getTokenTypeOp SemiColon, "OPERATOR")
        Assert.AreEqual(getTokenTypeOp (Comparison GreaterThanEq), "OPERATOR")

        // Literals
        Assert.AreEqual(getTypeName (IntLiteral 1 |> mapNumberLiteral), "INTLITERAL")
        Assert.AreEqual(getTypeName (FloatLiteral (1.0, "1.0") |> mapNumberLiteral), "FLOATLITERAL")

    [<Test>]
    let Float () =
        let fs = "0.01"
        readTokensInString (FParsec.Primitives.many pToken) "float literal" fs
        |> List.head
        |> equateTokens (FloatLiteral (float 0.01, fs) |> mapNumberLiteral)
        |> passFail

        let fs = ".01"
        readTokensInString (FParsec.Primitives.many pToken) "float literal" fs
        |> List.head
        |> equateTokens (FloatLiteral (float 0.01, fs) |> mapNumberLiteral)
        |> passFail

    [<Test>]
    let Int () =
        readTokensInString (FParsec.Primitives.many pToken) "int literal" "0"
        |> List.head
        |> equateTokens (IntLiteral 0 |> mapNumberLiteral)
        |> passFail

    [<Test>]
    let IdentifierCannotBeKeyword () =
        runParserOnString pIdentifier (new SymbolTableDict()) "" "RETURN"
        |> function
            | Success _ -> Assert.Fail()
            | Failure _ -> Assert.Pass()