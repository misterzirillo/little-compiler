namespace LittleCompiler.Test

open NUnit.Framework
open LittleCompiler.Parser.Comment
open FParsec

module CommentTest =

    let passFail =
        function
        | Success _ -> ()
        | Failure (e,r,_) -> failwith (sprintf "%O\n%O" e r)

    let testString s =
        new TestDelegate (fun () -> run (many1 pComment) s |> passFail)

    [<SetUp>]
    let Setup () =
        ()

    [<Test>]
    let RecognizeComment () =
        Assert.Throws (testString "- hello I'm not  comment") |> ignore
        Assert.Throws (testString "   -- hello I'm a cursed comment") |> ignore

    [<Test>]
    let NotRecognizeComment () =
        Assert.DoesNotThrow (testString "-- hello I'm a comment")
        Assert.DoesNotThrow (testString "--\n--hello\n-- I'm a very cursed comment") |> ignore