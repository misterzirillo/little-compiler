namespace LittleCompiler.Test

open FParsec
open NUnit.Framework
open LittleCompiler.Parser.Program
open LittleCompiler.Parser.Statement
open LittleCompiler.Parser.Token
open System.IO
open LittleCompiler

module ProgramTest =

    let passFail =
        function
        | Success (r,_,_) -> r
        | Failure (e,_,_) -> failwith e

    let parseProgram s = runParserOnString pProgram (new SymbolTableDict()) "" s |> passFail

    [<Test>]
    let ProgramBasic () =
        let r = parseProgram "PROGRAM p BEGIN END"
        let expected = {
            programName = "p"
            globalVars = []
            functions = []
        }
        Assert.AreEqual(expected, r)

    [<Test>]
    let ProgramAdvanced () =
        let r = parseProgram @"PROGRAM p BEGIN
                                STRING whatever := ""whatever"";

                                FUNCTION VOID f() BEGIN END

                                END"
        let expected = {
            programName = "p"
            globalVars = [
                GlobalString (
                    "whatever",
                    "whatever"
                )
            ]
            functions = [
                (
                    VOID,
                    "f",
                    [],
                    []
                )
            ]
        }
        Assert.AreEqual(expected, r)

    [<Test>]
    let ProvidedTestPrograms () =
        Assert.Multiple(fun () ->
            [1..21]
            |> List.iter(fun i -> 
                let expectedAccepted = (File.ReadAllText <| sprintf "Assets/Step2/outputs/test%d.out" i).TrimEnd()
                let parserResult = runParserOnFile pProgram (new SymbolTableDict()) (sprintf "Assets/Step2/inputs/test%d.micro" i) System.Text.Encoding.UTF8
                let (accepted, programOrError) = 
                    match parserResult with
                    | Success (r,_,_) -> 
                        ("Accepted", getLittleProgramAsString r)
                    | Failure (e,_,_) -> 
                        ("Not accepted", e)
                
                Assert.AreEqual(
                    expectedAccepted, accepted,
                    sprintf "Test program #%d was %s, expected %s\n%s\n" i accepted expectedAccepted programOrError
                )
            )
        )
