﻿namespace LittleCompiler.Test

open LittleCompiler
open FParsec
open LittleCompiler.Parser.Program
open NUnit.Framework
open System.IO

module SymbolTableTest = 
    let passFailUserState result =
        match result with
        | Success (_,s: SymbolTableDict,_) -> s
        | Failure (e,_,_) -> failwith e

    let parseProgram s = runParserOnString pProgram (new SymbolTableDict()) "" s |> passFailUserState

    [<Test>]
    let BlockTableNaming () =
        let dict = new SymbolTableDict()

        dict.AddTable "BLOCK"
        Assert.AreEqual("BLOCK 1", dict.CurrentTableName)

        dict.AddTable "BLOCK"
        Assert.AreEqual("BLOCK 2", dict.CurrentTableName)

    [<Test>]
    let EmptyGlobal () = 
        let dict = parseProgram "PROGRAM p BEGIN END"

        Assert.AreEqual(1, dict.Count,
            "Empty program has one global symbol table"
        )

        Assert.True(dict.ContainsKey("GLOBAL"))


    (*
    [<Test>]
    let Step3ProvidedTestPrograms () = 
        Assert.Multiple(fun () ->
            [1; 5; 6; 7; 8; 9; 11; 13; 14; 16; 18; 19; 20; 21]
            |> List.iter(fun n ->
                Step3Output.Reset

                printfn "Running parser on test file #%d" n
                let result = runParserOnFile pProgram (new SymbolTableStack()) (sprintf "Assets/Step3/inputs/test%d.micro" n) System.Text.Encoding.UTF8

                match result with
                | Success (r, s, _) -> ()
                | Failure (e, _, _) -> Assert.Fail (e)

                let expectedOutput = (File.ReadAllText <| sprintf "Assets/Step3/outputs/test%d.out" n).TrimEnd()

                Assert.AreEqual(expectedOutput, Step3Output.GetString.Trim(),
                    sprintf "Symbol table matches given output for file test%d.micro" n
                )

                printfn ""
            )
        )
    *)
