namespace LittleCompiler.Test

open NUnit.Framework
open LittleCompiler.Parser.Expressions
open LittleCompiler.Parser.Token
open FParsec.CharParsers
open LittleCompiler

module ExpressionTest =

    let parseExpression s =
        s
        |> runParserOnString pExpression (new SymbolTableDict()) ""
        |> failLoudly

    let toIntValueLiteral = 
        IntLiteral >> LittleValueLiteral.NumberLiteral >> ValueLiteralExpression

    let toFloatValueLiteral f =
        FloatLiteral f
        |> LittleValueLiteral.NumberLiteral
        |> ValueLiteralExpression


    let one = toIntValueLiteral 1

    [<Test>]
    let Int () =
        let r = parseExpression "12"
        Assert.AreEqual((toIntValueLiteral 12), r)

    [<Test>]
    let Float () =
        let r = parseExpression "12.0"
        Assert.AreEqual((toFloatValueLiteral (12.0,"12.0")), r)

    [<Test>]
    let IdentifierLiteral () =
        let r = parseExpression "somenumber"
        Assert.AreEqual("somenumber" |> IdentifierExpression, r)

    [<Test>]
    let AddOp () =
        let r = parseExpression "1 + 1"
        Assert.AreEqual(AddOperation (one, one) |> OperationExpression, r)

    [<Test>]
    let SubOp () =
        let r = parseExpression "1 - 1"
        Assert.AreEqual(SubtractOperation (one, one) |> OperationExpression, r)

    [<Test>]
    let MultOp () =
        let r = parseExpression "1 * 1"
        Assert.AreEqual(MultOperation (one, one) |> OperationExpression, r)

    [<Test>]
    let DivOp () =
        let r = parseExpression "1 / 1"
        Assert.AreEqual(DivOperation (one, one) |> OperationExpression, r)

    [<Test>]
    let BasicPrecedence () =
        let r = parseExpression "1 + 1 * 1"
        let expected = 
            AddOperation (
                one,
                MultOperation (one, one) |> OperationExpression
            ) |> OperationExpression
        Assert.AreEqual(expected, r)

        let r = parseExpression "1 / 1 * 1"
        let expected = 
            MultOperation (
                DivOperation (one, one) |> OperationExpression,
                one
            ) |> OperationExpression
        Assert.AreEqual(expected, r)

        let r = parseExpression "1 + 1 / 1 * 1"
        let expected =
            AddOperation (
                one,
                MultOperation (
                    DivOperation (one, one) |> OperationExpression,
                    one
                ) |> OperationExpression
            ) |> OperationExpression
        Assert.AreEqual(expected, r)

    [<Test>]
    let AdvancedPrecedence () =
        let addOnes = AddOperation (one,one) |> OperationExpression
        let subOnes = SubtractOperation (one,one) |> OperationExpression

        let r = parseExpression "(1 + 1) / 1"
        let expected =
            DivOperation (
                addOnes,
                one
            ) |> OperationExpression
        Assert.AreEqual(expected, r)

        let r = parseExpression "(1 + 1) / (1 - 1)"
        let expected = DivOperation (addOnes, subOnes) |> OperationExpression
        Assert.AreEqual(expected, r)

        let r = parseExpression "((1))"
        let expected = one
        Assert.AreEqual(expected, r)

    [<Test>]
    let WithIdentifier () =
        let identifier = IdentifierExpression "id"
        let expected = AddOperation (one, identifier) |> OperationExpression
        let r = parseExpression "1 + id"
        Assert.AreEqual(expected, r)

    [<Test>]
    let ConfoundingSpaces () =
        let identifier = IdentifierExpression "id"

        let expected = AddOperation (one, identifier) |> OperationExpression
        let r = parseExpression "1 +id"
        Assert.AreEqual(expected, r)

        let expected = AddOperation (one, identifier) |> OperationExpression
        let r = parseExpression "1+id"
        Assert.AreEqual(expected, r)

    [<Test>]
    let Call () =
        let expected = FunctionCallExpression ("add", List.singleton one)

        let r = parseExpression "add(1)"
        Assert.AreEqual(expected, r)

        let r = parseExpression "add ( 1 )"
        Assert.AreEqual(expected, r)

        let r = parseExpression "add ( 1 , 1 )"
        let expected = FunctionCallExpression ("add", [one; one])
        Assert.AreEqual(expected, r)

        let r = parseExpression "add ( 1 ,1)"
        let expected = FunctionCallExpression ("add", [one; one])
        Assert.AreEqual(expected, r)

    [<Test>]
    let MixedTest () =
        let identifier = IdentifierExpression "id"

        let r = parseExpression "( 1+1 )/id (id, 1)"
        let expected =
            DivOperation (
                AddOperation (
                    one,
                    one
                ) |> OperationExpression,
                FunctionCallExpression ("id", [identifier; one])
            ) |> OperationExpression

        Assert.AreEqual(expected, r)

        