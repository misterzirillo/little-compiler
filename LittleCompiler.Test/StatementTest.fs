namespace LittleCompiler.Test

open LittleCompiler.Parser.Statement
open LittleCompiler.Parser.Token
open LittleCompiler.Parser.Expressions
open NUnit.Framework
open FParsec
open LittleCompiler

module StatementTest =

    let passFail =
        function
        | Success (r,_,_) -> r
        | Failure (e,_,_) -> failwith e

    // a choice parser for testing
    let pStatement = pAnyStatement

    let parseOneStatement s = runParserOnString pStatement (new SymbolTableDict()) "" s |> passFail

    [<Test>]
    let VarDec () =
        let statement = "INT it ;"
        let r = parseOneStatement statement
        let expected =
            VarDeclaration (
                INT,
                ["it"]
            ) |> Declaration
        Assert.AreEqual(expected, r)

        let statement = "INT it, it ,it;"
        let r = parseOneStatement statement
        let expected =
            VarDeclaration (
                INT,
                ["it"; "it"; "it"]
            ) |> Declaration
        Assert.AreEqual(expected, r)

    [<Test>]
    let VarAssign () =
        let statement = "INT it ;"
        let r = parseOneStatement statement
        let expected =
            VarDeclaration (
                INT,
                ["it"]
            ) |> Declaration
        Assert.AreEqual(expected, r)

    [<Test>]
    let GlobalAssign () =
        let statement = "STRING it := \"string literal\";"
        let r = parseOneStatement statement
        let expected =
            GlobalString (
                "it",
                "string literal"
            ) |> Declaration
        Assert.AreEqual(expected, r)

    [<Test>]
    let Read () =
        let statement = "READ (id1,id2, id3 ) ;"
        let r = parseOneStatement statement
        let expected =
            ReadStatement ["id1"; "id2"; "id3"]
        Assert.AreEqual(expected, r)

    [<Test>]
    let Write () =
        let statement = "WRITE(id1,id2, id3 ) ;"
        let r = parseOneStatement statement
        let expected =
            WriteStatement ["id1"; "id2"; "id3"]
        Assert.AreEqual(expected, r)

    let valueLiteral2 = 2 |> IntLiteral |> LittleValueLiteral.NumberLiteral |> ValueLiteralExpression
    let identifier = IdentifierExpression "id"
    let assign2 = Assignment ("id", valueLiteral2)

    [<Test>]
    let If () =
        let statement = "IF (id < 2) ENDIF"
        let r = parseOneStatement statement
        let expected =
            IfStatement (
                (identifier, LessThan, valueLiteral2),
                [],
                None
            ) |> ControlStatement
        Assert.AreEqual(expected, r, "empty if")

        let statement = @"IF (id < 2)
                            id := 2;
                        ENDIF"
        let r = parseOneStatement statement
        let expected =
            IfStatement (
                (identifier, LessThan, valueLiteral2),
                [ assign2 ],
                None
            ) |> ControlStatement
        Assert.AreEqual(expected, r, "with statement")

        let statement = @"IF (id < 2)
                            id := 2;
                        ELSE
                            id := 2;
                        ENDIF"
        let r = parseOneStatement statement
        let expected =
            IfStatement (
                (identifier, LessThan, valueLiteral2),
                [ assign2 ],
                Some [ assign2 ]
            ) |> ControlStatement
        Assert.AreEqual(expected, r, "with else")

    [<Test>]
    let While () =
        let statement = "WHILE ( id >= 2) ENDWHILE"
        let r = parseOneStatement statement 
        let expected =
            WhileStatement (
                (identifier, GreaterThanEq, valueLiteral2),
                []
            ) |> ControlStatement
        Assert.AreEqual(expected, r, "empty while")

        let statement = @"WHILE ( id >= 2)
                            id :=2;
                        ENDWHILE"
        let r = parseOneStatement statement 
        let expected =
            WhileStatement (
                (identifier, GreaterThanEq, valueLiteral2),
                [assign2]
            ) |> ControlStatement
        Assert.AreEqual(expected, r, "empty while")