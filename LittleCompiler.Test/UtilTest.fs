namespace LittleCompiler.Test

module UtilTests =

    open NUnit.Framework
    open LittleCompiler.Test.Util
    open LittleCompiler.Parser.Token

    [<SetUp>]
    let Setup () =
        ()

    /// Smoke test to see if `.out` files are parsed
    [<Test>]
    let FibOut () =
        let tokens = readTokensOutFile "Assets/Step1/outputs/fibonacci.out"
        Assert.AreEqual(Program |> Keyword, tokens.Head)
        Assert.AreEqual(End |> Keyword, List.last tokens)
