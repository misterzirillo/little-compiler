namespace LittleCompiler.Test

open FParsec.CharParsers
open FParsec.Primitives
open LittleCompiler.Parser.Token
open LittleCompiler.Parser.Program
open LittleCompiler.Parser.Statement
open LittleCompiler

[<AutoOpen>]
module Util =

    let private pIgnoreMe = skipCharsTillString "Value:" true System.Int32.MaxValue
    let private pTokenValue = pIgnoreMe >>. between spaces spaces pToken
    let private outfile = many pTokenValue .>> eof

    let private infile = sepEndBy1 pToken spaces .>> eof
    let private filterComments =
        function
            | Literal l -> match l with CommentLiteral _ -> false | _ -> true
            | _ -> true

    /// Throws if the ParserResult is Failure, otherwise returns the parsed data
    let failLoudly r =
        match r with
        | Success (r, _, _) -> r
        | Failure (e, _, _) -> failwith e

    /// Read in the tokens from a particular `.out` file
    let readTokensOutFile filename =
        runParserOnFile outfile (new SymbolTableDict()) filename System.Text.Encoding.UTF8
        |> failLoudly

    /// Parse the tokens from a source file
    let readTokensInFile filename =
        runParserOnFile infile (new SymbolTableDict()) filename System.Text.Encoding.UTF8
        |> failLoudly
        |> List.filter filterComments

    /// Parse the tokens in a string
    let readTokensInString parser stringname str =
        runParserOnString parser (new SymbolTableDict()) stringname str
        |> failLoudly
        |> List.filter filterComments

    let getLittleProgramAsString (lp : LittleProgram) =
        let globalVars = 
            lp.globalVars
            |> List.map(fun v -> 
                match v with
                | VarDeclaration (varType, identifiers) -> 
                    sprintf "%s: %s" 
                        (string varType) 
                        (identifiers
                        |> List.map(fun i -> string i)
                        |> String.concat ", ")
                | GlobalString (identifier, content) -> 
                    sprintf "STRING %s: %s" identifier content
            )
            |> String.concat "; "

        let funcs =
            lp.functions
            |> List.map(fun (t, id, _, _) -> sprintf "%s %s" (string t) (string id))
            |> String.concat "; "

        sprintf "name: %s\nglobalVars: %s\nfuncs: %s\n" lp.programName globalVars funcs
