namespace LittleCompiler.Test

open LittleCompiler.Parser.Functions
open LittleCompiler.Parser.Statement
open LittleCompiler.Parser.Expressions
open LittleCompiler.Parser.Token
open FParsec
open NUnit.Framework
open LittleCompiler

module FunctionsTest =

    let passFail =
        function
        | Success (r,_,_) -> r
        | Failure (e,_,_) -> failwith e

    let parseFunc s = runParserOnString pFunctionDeclaration (new SymbolTableDict()) "" s |> passFail

    let identifier = "id" 

    [<Test>]
    let FuncDeclare () =
        let f = "FUNCTION INT id () BEGIN END"
        let r = parseFunc f
        let expected =
            LittleFunctionDeclaration (
                INT |> VarType, 
                identifier,
                [],
                []
            )
        Assert.AreEqual(expected, r)

        let f = @"FUNCTION INT id ( INT id, INT id ,FLOAT id ) BEGIN
                    id := id;
                END"
        let r = parseFunc f
        let expected =
            LittleFunctionDeclaration (
                INT |> VarType,
                identifier,
                [
                    (INT, identifier)
                    (INT, identifier)
                    (FLOAT, identifier)
                ],
                [
                    Assignment (
                        identifier,
                        identifier |> IdentifierExpression
                    )
                ]
            )
        Assert.AreEqual(expected, r)