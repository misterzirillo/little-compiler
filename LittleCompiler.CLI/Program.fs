﻿open System
open LittleCompiler
open LittleCompiler.Parser.Statement
open LittleCompiler.CLI.CommandLineParser
open System.IO
open FParsec

[<EntryPoint>]
let main argv = 
    let options = CommandLineParser.parseCommandLine (argv |> List.ofArray)

    if String.IsNullOrWhiteSpace options.inFile then
        eprintfn "No input file specified."
        1
    else if not <| File.Exists options.inFile then
        eprintfn "Input file \"%s\" not found and can't be read." 
            (Path.GetFullPath options.inFile)
        1
    else

    try
        let (program, symbolTables) = LittleCompiler.parseMicroFile options.inFile
        
        let tinyCode = LittleCompiler.getTinyFromAST program symbolTables

        if options.stdOut then
            printf "%s" tinyCode
        
        if options.outFile <> "" then
            File.WriteAllText(options.outFile, tinyCode)

    with
        | e -> printf "Error %s" e.StackTrace

    0 // return an integer exit code
    