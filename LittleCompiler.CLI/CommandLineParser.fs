﻿namespace LittleCompiler.CLI.CommandLineParser

module CommandLineParser = 
    type CommandLineOptions = {
        stdOut: bool;
        inFile: string;
        outFile: string;
        }

    let rec parseCommandLineRec args optionsSoFar = 
        match args with 
        // Base case
        | [] -> 
            optionsSoFar

        // Print to standard out flag
        | "-s"::xs -> 
            let newOptionsSoFar = { optionsSoFar with stdOut=true }
            parseCommandLineRec xs newOptionsSoFar

        // Input file flag
        | "-i"::xs -> 
            match xs with
            | filePath::xss -> 
                let newOptionsSoFar = { optionsSoFar with inFile=filePath }
                parseCommandLineRec xss newOptionsSoFar
            | _ -> 
                eprintfn "-i requires a filename"
                parseCommandLineRec xs optionsSoFar

        // Output file flag
        | "-o"::xs -> 
            match xs with
            | filePath::xss -> 
                let newOptionsSoFar = { optionsSoFar with outFile=filePath }
                parseCommandLineRec xss newOptionsSoFar
            | _ -> 
                eprintfn "-o requires a filename"
                parseCommandLineRec xs optionsSoFar

        | x::xs -> 
            eprintfn "Option '%s' is unrecognized" x
            parseCommandLineRec xs optionsSoFar 

    let parseCommandLine args = 
        // Default options
        let defaultOptions = {
            stdOut = false;
            inFile = "";
            outFile = "";
            }

        // Pass off to recursive
        parseCommandLineRec args defaultOptions
